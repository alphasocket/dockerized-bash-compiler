FROM alpine:latest
RUN apk add --no-cache git gcc make musl-dev \
    && git -C $HOME clone https://github.com/neurobin/shc && cd $HOME/shc && ./configure && make && make install && shc --help \
    && apk del gcc musl-dev
